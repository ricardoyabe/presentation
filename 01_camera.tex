\section{Dependency injection}

\subsection{Motivation}

\begin{frame}
  Let's build a computer vision system that:
  
  \begin{enumerate}
    \item Captures the video stream from a camera
    \item Detects human faces
    \item Post the number of people to a web service
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \begin{lstlisting}
import cv2
import requests


def process_faces():
    face_cascade = cv2.CascadeClassifier(
        'haarcascade_frontalface_default.xml')
    cap = cv2.VideoCapture(0)

    for _ in range(10):
        _, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.1, 4)
        number_of_faces = len(faces)
        requests.post('http://127.0.0.1:8000/count',
                      data={'count': number_of_faces})

    cap.release()
  \end{lstlisting}


\end{frame}

\subsection{Problems}

\begin{frame}[fragile]
  
  Problems?

  \begin{lstlisting}
import cv2
import requests


def process_faces():
    face_cascade = cv2.CascadeClassifier(
        'haarcascade_frontalface_default.xml')
    cap = cv2.VideoCapture(0)

    for _ in range(10):
        _, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.1, 4)
        number_of_faces = len(faces)
        requests.post('http://127.0.0.1:8000/count',
                      data={'count': number_of_faces})

    cap.release()
      \end{lstlisting}
  
\end{frame}

\begin{frame}[fragile]
  
  Problems?

  \begin{lstlisting}
import cv2
import requests


def process_faces():
    face_cascade = cv2.CascadeClassifier(
        'haarcascade_frontalface_default.xml')
    cap = cv2.VideoCapture(0)

    for _ in range(10):
        _, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.1, 4)
        number_of_faces = len(faces)
        requests.post('http://127.0.0.1:8000/count',
                      data={'count': number_of_faces})

    cap.release()
      \end{lstlisting}

    The \texttt{process\_faces()} method depends on concrete implementations from \emph{cv2} and \emph{requests}.
  
\end{frame}

\begin{frame}[fragile]
  Consequences: Testing?

  \begin{itemize}
    \item mock \emph{cv2}
    \item mock \emph{requests}
  \end{itemize}

  \begin{lstlisting}
    import cv2
    import requests
    
    
    def process_faces():
        face_cascade = cv2.CascadeClassifier(
            'haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)
    
        for _ in range(10):
            _, img = cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.1, 4)
            number_of_faces = len(faces)
            requests.post('http://127.0.0.1:8000/count',
                          data={'count': number_of_faces})
    
        cap.release()
          \end{lstlisting}

    \uncover<2->{Do not mock any class you do not own.}
\end{frame}

\begin{frame}[fragile]
  Consequences: New feature? Replacing algorithm?

  \begin{itemize}
    \item Processing images from video file
    \item Deep learning instead of Haar Cascade
    \item Saving the number of faces to a log file
  \end{itemize}

  \begin{lstlisting}
    import cv2
    import requests
    
    
    def process_faces():
        face_cascade = cv2.CascadeClassifier(
            'haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)
    
        for _ in range(10):
            _, img = cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.1, 4)
            number_of_faces = len(faces)
            requests.post('http://127.0.0.1:8000/count',
                          data={'count': number_of_faces})
    
        cap.release()
          \end{lstlisting}
\end{frame}

\subsection{Solution}

\begin{frame}[fragile]
    Solution from SOLI\underline{D}: "Depend upon abstractions, not concretions."

    \cite{wiki:solid}

    \begin{lstlisting}
class AbstractImageProducer(ABC):
@abstractmethod
def get_image(self) -> np.ndarray: ...

class AbstractFaceCountDetector(ABC):
@abstractmethod
def count_faces(self, image: np.ndarray) -> int: ...

class AbstractPeopleCountHandler(ABC):
@abstractmethod
def process_people_count(self, number_of_people: int) -> None: ...

    \end{lstlisting}
  
\end{frame}

\begin{frame}[fragile]

  Now \texttt{process\_faces()} depends on abstract classes:

\begin{lstlisting}
from input import AbstractImageProducer
from output import AbstractPeopleCountHandler
from processor import AbstractFaceCountDetector


def process_faces(image_producer: AbstractImageProducer,
                  face_detector: AbstractFaceCountDetector,
                  people_count_handler: AbstractPeopleCountHandler):
    for _ in range(10):
        img = image_producer.get_image()
        number_of_faces = face_detector.count_faces(img)
        people_count_handler.process_people_count(number_of_faces)
\end{lstlisting}

\end{frame}


\begin{frame}[fragile]

  Camera implementation:

\begin{lstlisting}
import cv2
import numpy as np

from input import AbstractImageProducer


class CameraImageProducer(AbstractImageProducer):
  def __init__(self, camera_id: int):
      self.camera_id = camera_id

  def open(self):
      self.cap = cv2.VideoCapture(self.camera_id)
      if not self.cap.isOpened():
          raise RuntimeError('Unable to open camera: {}'.format(self.camera_id))

  def close(self) -> None:
      self.cap.release()

  def get_image(self) -> np.ndarray:
      if self.cap.isOpened():
          ret, frame = self.cap.read()
          if ret:
              return frame
          else:
              raise RuntimeError('Invalid ret: {}'.format(ret))
      else:
          raise RuntimeError('Camera is not opened')

\end{lstlisting}

\end{frame}

\begin{frame}[fragile]

  Counting faces implementation:

\begin{lstlisting}
import cv2
import numpy as np

from processor import AbstractFaceCountDetector


class CV2FaceCountDetector(AbstractFaceCountDetector):
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier(
          'haarcascade_frontalface_default.xml')

    def count_faces(self, image: np.ndarray) -> int:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)
        return len(faces)
  
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]

  Output implementation:

\begin{lstlisting}
import requests

from output import AbstractPeopleCountHandler


class RPCPeopleCountHandler(AbstractPeopleCountHandler):
    def __init__(self, url: str, port: int, path: str):
        self.url = 'http://{}:{}/{}'.format(url, port, path)

    def process_people_count(self, number_of_faces: int) -> None:
        requests.post(self.url, data={'count': number_of_faces})
\end{lstlisting}

\end{frame}

\subsection{Dependency injection}

\begin{frame}
  How to put everything together? 

  \uncover<2->{Dependency injection! \cite{fowler:2004}} 

  \uncover<3->{
    The basic idea of the Dependency Injection is to have a separate object, an
    assembler, that populates a field with an appropriate implementation for the
    given interface or abstract class.
  }

  \uncover<4->{
    (Or argument: we have lambdas)
  }

\end{frame}

\begin{frame}[fragile]
  \begin{lstlisting}
from input.camera import CameraImageProducer
from face_detector import process_faces
from output.rpc import RPCPeopleCountHandler
from processor.cv2 import CV2FaceCountDetector


def main():
    # Building the concrete implementations
    cip = CameraImageProducer(0)
    cip.open()

    cv2processor = CV2FaceCountDetector()

    # 127.0.0.1:8000/count
    rpc = RPCPeopleCountHandler('127.0.0.1', 8000, 'count')

    # Injection
    process_faces(image_producer=cip,
                  face_detector=cv2processor,
                  people_count_handler=rpc)

    # Someone must deal with the implementation details
    cip.close()

if __name__ == '__main__':
    main()
  \end{lstlisting}
\end{frame}

\begin{frame}
  
  In this example, the assembler was simple. We will discuss a more complex
  implementation later.  

\end{frame}